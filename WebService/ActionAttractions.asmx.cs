﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using MemcachedProviders.Cache;

namespace ActionAttractions
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class ActionAttractions : System.Web.Services.WebService
    {
        [WebMethod]

        public string GetActionAttractions(string startDate, int numRecords, Parameters.enEventType enEventType, Parameters.enDaysFwd enDaysFwd, Parameters.enSportCat enSportCat, Parameters.enChannel enChannel, Parameters.enRegion enRegion, Parameters.enOrder enOrder)
        {
            string cachekey = startDate + "_" + numRecords.ToString() + "_" + enEventType.ToString() + "_" + enDaysFwd + "_" + enSportCat.ToString() + "_" + enChannel.ToString() + "_" + enRegion + "_" + enOrder.ToString();
            string str = "";

            str = Convert.ToString(CacheProvider.memCacheGetObject(cachekey));

            if (string.IsNullOrEmpty(str))
            {
                string query = "";

                string vType = Convert.ToString(enEventType);
                int vDays = Convert.ToInt32(enDaysFwd);
                string vSport = Convert.ToString(enSportCat);
                string vChannel = Convert.ToString(enChannel);
                int vRegion = Convert.ToInt32(enRegion);
                int vOrder = Convert.ToInt32(enOrder);

                if (vDays == -1)
                {
                    vDays = 0;
                }

                string vOrderBy = "";

                switch (vOrder)
                {
                    case 0:
                        vOrderBy = " SortOrder, aa_chan, aa_date ";
                        break;

                    case 1:
                        vOrderBy = " aa_date, aa_chan ";
                        break;

                    case 2:
                        vOrderBy = " aa_chan, aa_date ";
                        break;

                    case 3:
                        vOrderBy = " aa_date ";
                        break;
                }

                string evType = "";

                switch (vType)
                {
                    case "All":
                        evType = "*";
                        break;

                    case "Live":
                        evType = "L";
                        break;

                    case "Repeat":
                        evType = "R";
                        break;

                    case "Premier":
                        evType = "P";
                        break;

                    case "Delayed":
                        evType = "D";
                        break;

                    case "LiveNow":
                        evType = "LIVENOW";
                        break;
                }

                DateTime vDateStart = Convert.ToDateTime(startDate);
                DateTime vDateEnd = vDateStart.AddDays(vDays);

                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);

                query = "";

                if (numRecords != 0)
                {
                    query += "SELECT top " + numRecords + " ";
                }
                else
                {
                    query += "SELECT ";
                }
                query += "tSCH.aa_event_id as vEVENTID, ";
                query += "tSCH.aa_date as vDATE, ";
                query += "tSCH.aa_start as vSTART, ";
                query += "tSCH.aa_end as vEND, ";
                query += "tSCH.aa_sport as vSPORT, ";
                query += "(CASE When tSCH.aa_channel = 'SUPERSPORT SELECT SA' Then 'SuperSport Select SA' Else tSCH.aa_channel End) as vCHANNEL, ";
                query += "tSCH.aa_chan as vCHAN, ";
                query += "tSCH.aa_type as vTYPE, ";
                query += "tSCH.aa_title_web as vTITLE, ";
                query += "tSCH.aa_detail_web as vDETAIL, ";
                query += "tSCH.aa_title as vTITLEACTIVE, ";
                query += "tSCH.aa_detail as vDETAILACTIVE, ";

                query += "(Case When tSCH.aa_chan='SHD' Then 1 ";
                query += "When tSCH.aa_chan='SH2' Then 2 ";
                query += "When tSCH.aa_chan='SHD3' Then 3 ";
                query += "When tSCH.aa_chan='SHD4' Then 4 ";
                query += "When tSCH.aa_chan='HD5' Then 5 ";
                query += "When tSCH.aa_chan='SHD5' Then 5 ";
                query += "When tSCH.aa_chan='SHD6' Then 6 ";
                query += "When tSCH.aa_chan='HD7' Then 7 ";
                query += "When tSCH.aa_chan='SHD7' Then 7 ";
                query += "When tSCH.aa_chan='HD3N' Then 8 ";
                query += "When tSCH.aa_chan='SS1' Then 9 ";
                query += "When tSCH.aa_chan='SS1A' Then 10 ";
                query += "When tSCH.aa_chan='SS2' Then 11 ";
                query += "When tSCH.aa_chan='SS2A' Then 12 ";
                query += "When tSCH.aa_chan='SS3' Then 13 ";
                query += "When tSCH.aa_chan='SS3A' Then 14 ";
                query += "When tSCH.aa_chan='SS3N' Then 15 ";
                query += "When tSCH.aa_chan='SS4' Then 16 ";
                query += "When tSCH.aa_chan='SS5' Then 17 ";
                query += "When tSCH.aa_chan='SS5A' Then 18 ";
                query += "When tSCH.aa_chan='SS5N' Then 19 ";
                query += "When tSCH.aa_chan='SS6' Then 20 ";
                query += "When tSCH.aa_chan='SS6A' Then 21 ";
                query += "When tSCH.aa_chan='SS7' Then 22 ";
                query += "When tSCH.aa_chan='SS7N' Then 23 ";
                query += "When tSCH.aa_chan='SS8' Then 24 ";
                query += "When tSCH.aa_chan='SS9' Then 25 ";
                query += "When tSCH.aa_chan='SS10' Then 26 ";
                query += "When tSCH.aa_chan='SSB' Then 27 ";
                query += "When tSCH.aa_chan='SSM' Then 28 ";
                query += "When tSCH.aa_chan='SSM2' Then 29 ";
                query += "When tSCH.aa_chan='SSA' Then 30 ";
                query += "When tSCH.aa_chan='HD' Then 31 ";
                query += "When tSCH.aa_chan='MNET' Then 32 ";
                query += "When tSCH.aa_chan='CSN' Then 33 ";
                query += "When tSCH.aa_chan='SSL' Then 34 ";
                query += "When tSCH.aa_chan='SLSA' Then 35 ";
                query += "When tSCH.aa_chan='SS11' Then 35 ";
                query += "When tSCH.aa_chan='SS12' Then 36 ";
                query += "Else 100 End) AS vRANK, ";

                query += "(Case When tSCH.aa_chan like 'S%' Then 0 Else 1 End) As SortOrder ";

                query += "FROM SuperSport.dbo.aa_entries tSCH ";

                if (evType == "LIVENOW")
                {
                    query += "WHERE( ('" + DateTime.Now + "' between tSCH.aa_start and tSCH.aa_end) ";

                    if (vSport == "Olympics")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%Olympics%') "; }
                    }
                    else if (vSport == "Paralympics")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%Paralympics%') "; }
                    }
                    else if (vSport == "Commonwealth")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%Commonwealth%') "; }
                    }
                    else if (vSport == "WWE")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%WWE%') "; }
                    }
                    else
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sportcategory='" + vSport + "') "; }
                    }
                    if (vChannel != "All") { query += "AND (tSCH.aa_chan='" + vChannel + "') "; }
                    if (vRegion != 0) { query += "AND (tSCH.aa_region_sub_" + vRegion + "= 1) "; }
                    if (numRecords > 0) { query += "AND (NOT tSCH.aa_chan='SSB') "; } // EXCLUDE ALL BLITZ ENTRIES

                    query += ") ORDER BY vRANK";
                }
                else
                {
                    query += "WHERE(( (tSCH.aa_start BETWEEN '" + vDateStart.ToString("yyyy-MM-dd HH:mm:ss") + "' AND '" + vDateEnd.ToString("yyyy-MM-dd 23:59:59") + "')  OR (tSCH.aa_start <= '" + vDateStart.ToString("yyyy-MM-dd HH:mm:ss") + "' AND  tSCH.aa_end>='" + vDateStart.ToString("yyyy-MM-dd HH:mm:ss") + "' ) )";

                    if (evType != "*") { query += "AND (tSCH.aa_type='" + evType + "') "; }
                    if (vSport == "Olympics")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%Olympics%') "; }
                    }
                    else if (vSport == "Paralympics")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%Paralympics%') "; }
                    }
                    else if (vSport == "Commonwealth")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%Commonwealth%') "; }
                    }
                    else if (vSport == "WWE")
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sport LIKE '%WWE%') "; }
                    }
                    else
                    {
                        if (vSport != "All") { query += "AND (tSCH.aa_sportcategory='" + vSport + "') "; }
                    }
                    if (vChannel != "All") { query += "AND (tSCH.aa_chan='" + vChannel + "') "; }
                    if (vRegion != 0) { query += "AND (tSCH.aa_region_sub_" + vRegion + "= 1) "; }
                    if (numRecords > 0) { query += "AND (NOT tSCH.aa_chan='SSB') "; } // EXCLUDE ALL BLITZ ENTRIES

                   //query += "AND ((tSCH.aa_end >= '" + DateTime.Now + "')) "; // EXCLUDE ANY EVENTS EARLIER THAN NOW EXCEPT THOSE WHO ARE ON AIR AT THE MOMENT

                    query += ")AND (tSCH.aa_chan <> 'SIN3') AND (tSCH.aa_chan <> 'SIN4') AND (tSCH.aa_chan <> 'SIN5') AND (tSCH.aa_chan <> 'SIN6') AND (tSCH.aa_chan <> 'SIN7') AND (tSCH.aa_chan <> 'SMB1') AND (tSCH.aa_chan <> 'SMB2') AND (tSCH.aa_chan <> 'SMB3') AND (tSCH.aa_chan <> 'SSMW') ORDER BY " + vOrderBy;
                }

                dbConn.Open();

                SqlCommand sqlQuery = new SqlCommand(query, dbConn);
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlQuery;

                da.Fill(ds, "tblAction");

                dbConn.Close();

                MemoryStream ms = new MemoryStream();

                ds.WriteXml(ms, XmlWriteMode.IgnoreSchema);

                ms.Seek(0, SeekOrigin.Begin);

                StreamReader sr = new StreamReader(ms);

                str = sr.ReadToEnd();

                CacheProvider.memCacheAddObject(str, cachekey);
            }

            return str;

        }
    }
}