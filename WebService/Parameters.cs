﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace ActionAttractions
{
    public class Parameters
    {
        public enum enEventType
        {
            All = 0,
            Live = 1,
            Repeat = 2,
            Premier = 3,
            Delayed = 4,
            LiveNow = 5
        }

        public enum enDaysFwd
        {
            All = -1,
            Days_0 = 0,
            Days_1 = 1,
            Days_2 = 2, 
            Days_3 = 3,
            Days_4 = 4,
            Days_5 = 5,
            Days_6 = 6,
            Days_7 = 7,
            Days_8 = 8,
            Days_9 = 9,
            Days_10 = 10,
            Days_11 = 11,
            Days_12 = 12,
            Days_13 = 13,
            Days_14 = 14,
            Days_15 = 15,
            Days_16 = 16,
            Days_17 = 17,
            Days_18 = 18,
            Days_19 = 19,
            Days_20 = 20,
            Days_21 = 21
        }

        public enum enSportGroup
        {
            All = 0,
            General = 1,
            CommonwealthGames = 2,
            SummerOlympics = 3,
            WinterOlympics = 4
        }

        public enum enSportCat
        {
            All = 0,
            Aquatics = 1,
            Athletics = 2,
            Basketball = 3,
            Boxing = 4,
            Cricket = 5,
            Cycling = 6,
            Football = 7,
            General = 8,
            Golf = 9,
            Hockey = 10,
            Motorsport = 11,
            Rugby = 12,
            Tennis = 13,
            Olympics = 14,
            Paralympics = 15,
            AmericanFootball = 16,
            Baseball = 17,
            IceHockey = 18,
            MixedMartialArts = 19,
            Netball = 20,
            Wrestling = 21,
            Commonwealth = 22,
            WWE = 23
        }

        public enum enChannel
            {
                All = 0,
                SH2 = 1,
                SHD3 = 2,
                SHD4 = 3,
                HD5 = 4,
                SHD6 = 5,
                SHD7 = 6,
                HD3N = 7,
                SS1 = 8,
                SS1A = 9,
                SS2 = 10,
                SS2A = 11,
                SS3 = 12,
                SS3A = 13,
                SS3N = 14,
                SS4 = 15,
                SS5 = 16,
                SS5A = 17,
                SS5N = 18,
                SS6 = 19,
                SS6A = 20,
                SS7 = 21,
                SS7N = 22,
                SS8 = 23,
                SS9 = 24,
                SS10 = 25,
                SSB = 26,
                SSM = 27,
                SSM2 = 28,
                SSA = 29,
                HD = 30,
                MNET = 31,
                CSN = 32,
                SSL = 33,
                SS11 = 34,
                SS12 = 35
            }

        //public enum enRegion
        //{
        //    All = 0,
        //    SouthAfrica = 1,
        //    SouthAfrica_Nigeria_RestOfAfrica = 2,
        //    SouthAfrica_Namibia_Botswana_Mozambique_Zimbabwe_Lesotho_Swaziland = 3,
        //    Nigeria_RestOfAfrica = 4,
        //    RestOfAfrica = 5,
        //    Nigeria = 6,
        //    Portugese1 = 7,
        //    Portugese2_Angola_Mozambique = 8
        //}

        public enum enRegion
        {
            All = 0,
            South_Africa = 1,
            Nigeria = 2,
            Rest_of_Africa = 3,
            Southern_Africa = 4,
            Portugese = 5
        }

        public enum enOrder
        {
            SS_Priority = 0,
            Date_Channel = 1,
            Channel_Date = 2,
            Date = 3
        }
    }
}
