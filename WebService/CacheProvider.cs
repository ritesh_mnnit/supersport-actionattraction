﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MemcachedProviders.Cache;

namespace ActionAttractions
{
    public class CacheProvider
    {
        private const string key_suffix = "aa";

        public CacheProvider()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void removeAll()
        {
            DistCache.RemoveAll();
        }

        public static void memCacheAddObject(Object obj, string Key)
        {
            Key += key_suffix;
            DistCache.Add(Key, obj, true);
        }

        public static object memCacheGetObject(string Key)
        {
            Key += key_suffix;
            object obj = (object)DistCache.Get(Key);
            return obj;
        }

        public static void memCacheAddObject(Object obj, string Key, int Minutes)
        {
            Key += key_suffix;
            TimeSpan tmpSpan = new TimeSpan(0, Minutes, 0);
            DistCache.Add(Key, obj, tmpSpan);
        }
    }
}
