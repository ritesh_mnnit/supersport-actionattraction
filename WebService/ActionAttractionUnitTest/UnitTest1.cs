﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ActionAttractionUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            ActionAttractionServiceRef.ActionAttractionsSoapClient obj = new ActionAttractionServiceRef.ActionAttractionsSoapClient();
            string returnXml = obj.GetActionAttractions(DateTime.Now.ToString("yyyy-MM-dd"), 0, ActionAttractionServiceRef.enEventType.All, ActionAttractionServiceRef.enDaysFwd.Days_0,ActionAttractionServiceRef.enSportCat.All,ActionAttractionServiceRef.enChannel.SS1, ActionAttractionServiceRef.enRegion.Southern_Africa, ActionAttractionServiceRef.enOrder.Date);
            Console.WriteLine(returnXml);
            Console.ReadLine();
        }
    }
}
