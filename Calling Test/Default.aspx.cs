﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        action.ActionAttractions aatest = new action.ActionAttractions();

        string testval = aatest.GetActionAttractions("2012-07-16",0,
                                                     action.enEventType.All,
                                                     action.enDaysFwd.Days_21,
                                                     action.enSportCat.Olympics,
                                                     action.enChannel.All,
                                                     action.enRegion.South_Africa,
                                                     action.enOrder.SS_Priority);

        Response.Write(testval);
    }
}